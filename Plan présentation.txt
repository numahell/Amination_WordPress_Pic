Plan présentation

1 - Qu'est ce que WordPress ?
2 - Découverte rapide de la partie Administration
3 - Les articles (posts)
4 - Les catégories et étiquettes
5 - L'éditeur de texte
6 - La gestion des images
7 - Intégrer des éléments externes (réseaux sociaux)
8 - Les Menus
9 - Les pages
10 - La gestion des utilisateurs
11 - Les commentaires
12 - Intégrer des Widgets

Illustrations pour le site du Pic : https://wordpress.org/about/logos/

Présentation pour la page du Pic :

Avec 29% de sites qui l'utilise en 2017, WordPress est l'un des CMS (Content Managment System ou  Système de gestion de contenu) les plus utilisé au monde.
Gratuit, modulaire, mené par une vaste communauté, WordPress est maintenant devenu un outil incontournable qui a largement fait ses preuves.
Nous allons vous montrer qu'avec cet outil, créer votre site web associatif, n'a rien de bien compliqué ...

Objectif : Détailler la rédaction d'articles et organiser le contenu, tout en personnalisant la présentation et les fonctionnalités du site sous WordPress

Pré-requis : Posséder les notions de fichiers et de répertoires, savoir utiliser un navigateur Internet.

Matériel : Apporter sa machine personnelle, portable de préférence, équipée d’un carte réseau filaire ou WiFi, et en état de marche.